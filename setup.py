#!/usr/bin/env python
import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from rooster import __version__


readme_f = open(os.path.join(os.path.dirname(__file__), 'README.rst'))
long_description = readme_f.read()
readme_f.close()

setup(
    name='rooster',
    description='Python client for Rooster alerts.',
    long_description=long_description,

    url='http://github.com/heewa/rooster-py',
    version=__version__,

    author='Heewa Barfchin',
    author_email='heewa.b@gmail.com',
    license='MIT',

    packages=['rooster'],

    keywords=['Rooster', 'alert', 'notification'],
    classifiers=[
        'Development Status :: 3 - Alpha',

        'License :: OSI Approved :: MIT License',
        'Environment :: Console',
        'Intended Audience :: Developers',

        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
)
