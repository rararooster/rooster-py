from rooster.client import RoosterClient, initialize, rooster

__version__ = '0.0.1'
VERSION = tuple(int(num) for num in __version__.split('.'))

__all__ = [
    'RoosterClient',
    'initialize',
    'rooster',
]
