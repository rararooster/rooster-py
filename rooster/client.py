import re
from urllib import urlencode
from urllib2 import urlopen
import logging
from threading import Thread
import Queue


def initialize(
        key, host='in.rararooster.com',
        async=True, log=True, queue_size=4 * 1024):
    rooster.key = key
    rooster._host = host
    rooster._should_log = log
    rooster.async = async
    rooster.queue_size = queue_size


class RoosterClient(object):
    """Rooster alerting client.
    """

    _API_VERSION = 1

    def __init__(
            self, key, host='rararooster.com',
            async=True, log=True, queue_size=4 * 1024):
        """Create a client, connecting to the server and pulling down config.

        @param key: A key to use for alerts, unless overriden at alert time.
        @param host: RaRaRooster ingestion host.
        @param async: Bool. If True, alerts are queued for a separate thread
            to acutally deliver. If False, the alert call is blocking.
        @param log: Bool. If True, log warnings and errors.
        @param queue_size: If async, this is how many alerts can pile up
            between delivieries. If you're getting dropped alerts, increase
            this. If you're trying to save memory, and you're getting dropped
            alerts (meaning a full queue, that's actually using memory), and
            you don't mind dropping even more alerts, reduce this.
        """
        if key is None:
            self._key = None
        else:
            self.key = key
        self._host = host

        self._should_log = log

        self._alert_queue = Queue.Queue(maxsize=queue_size)

        self._thread = None
        self.async = async

        # counters
        self._alerts_seen = 0
        self._alerts_sent = 0
        self._alerts_dropped = 0
        self._delivery_failures = 0
        self._requests_made = 0

    @property
    def client_stats(self):
        compression_perc = 0
        if self._alerts_sent > 0:
            compression_perc = 100.0 * self._requests_made / self._alerts_sent

        drop_perc = 0
        if self._alerts_dropped:
            drop_perc = 100.0 * self._alerts_dropped / self._alerts_seen

        delivery_failure_perc = 0
        if self._delivery_failures:
            delivery_failure_perc = (
                100.0 * self._delivery_failures / self._alerts_seen)

        alerts_queued = (
            self._alerts_seen - self._alerts_sent - self._alerts_dropped)

        queue_util = 0
        if self._alert_queue.maxsize > 0 and self._alert_queue.qsize():
            queue_util = (
                100.0 * self._alert_queue.qsize() / self._alert_queue.maxsize)

        return {
            'alerts_seen': self._alerts_seen,
            'alerts_sent': self._alerts_sent,

            'alerts_queued': alerts_queued,
            'alert_queue_utilization': queue_util,

            'requests_made': self._requests_made,
            'compression_perc': compression_perc,

            'alerts_dropped': self._alerts_dropped,
            'drop_perc': drop_perc,

            'delivery_failures': self._delivery_failures,
            'delivery_failure_perc': delivery_failure_perc,
        }

    @property
    def key(self):
        return self._key

    @property
    def queue_size(self):
        return int(self._alert_queue.maxsize)

    @queue_size.setter
    def queue_size(self, queue_size):
        if self._alert_queue.maxsize == queue_size:
            return
        if not self._alert_queue.empty():
            raise Exception('Can\'t change queue size while items are queued.')
        self._alert_queue = Queue.Queue(maxsize=queue_size)

    @property
    def async(self):
        return self._thread is not None

    @async.setter
    def async(self, async):
        if async and not self._thread:
            self._thread = Thread(target=self._run)
            self._thread.daemon = True
            self._thread.start()
        elif not async and self._thread:
            # Just let go of the Thread reference, and let it finish up whatever
            # it's doing. But from now on, alerts will block.
            # TODO: signal for it to finish up and exit
            self._thread = None

    @key.setter
    def key(self, new_key):
        if not isinstance(new_key, basestring):
            raise TypeError('Key must be a %s type' % basestring.__name__)

        key_err = ValueError('Invalid key, get one from your dashboard.')

        if len(new_key) == 48:
            try:
                new_key = new_key.decode('hex')
            except (TypeError, ValueError):
                raise key_err
        elif len(new_key) != 24:
            raise key_err

        try:
            self._key = new_key.encode('hex')
        except (TypeError, ValueError):
            raise key_err

    def alert(self, project_id, name, msg=None):
        """Trigger an alert, which will send you a notification *if* you
        haven't yet been notified, or if you've cleared it since the laste
        one.

        @param project_id: Like 'org/project'. The key that was given when
            this client was created needs to have permission to this project.
        @param name: A unique identifier for the alert. Only one alert sent
            with this name will actually trigger a notification.
        @param msg: Optional. A description of the alert. This is only stored
            the first time this alert is seen on a stream.
        """
        if not self._key:
            raise ValueError('Need to give Rooster a key to send alerts.')

        self._alerts_seen += 1

        if self._thread:
            try:
                self._alert_queue.put((project_id, name, msg), block=False)
            except Queue.Full:
                self._alerts_dropped += 1
                if self._should_log:
                    logging.warning(
                        'Rooster Alert Queue full, dropping alert %s on %s.',
                        name, project_id)
        else:
            try:
                self._dispatch_alert(project_id, name, msg, 1)
            except Exception, err:
                if self._should_log:
                    logging.error(
                        'Failed to deliver alert %s on %s: %s',
                        name, project_id, err)

    def flush(self):
        """Send all queued alerts. Blocks until they're sent or errors are
        thrown.
        """
        raise NotImplementedError

    def __getattr__(self, org_name):
        """Get a Org.
        """
        return Org(self, org_name)

    def __getitem__(self, org_name):
        """Get a Org.
        """
        return getattr(self, org_name)

    def _dispatch_alert(self, project_id, name, msg, count):
        org, project = project_id.split('/')

        args = {
            'p': project_id,
            'a': name,
            'k': self._key,
            'c': count,
            'v': self._API_VERSION,
        }
        if msg and isinstance(msg, unicode):
            args['m'] = msg[:512].encode('utf-8')
        elif msg:
            args['m'] = str(msg)[:512]
        query = urlencode(args)

        url = 'http://%s.%s/alert?%s' % (org, self._host, query)

        reply = urlopen(url)
        reply.read()
        reply.close()

        self._alerts_sent += count
        self._requests_made += 1

    def _run(self):
        while True:
            try:
                self._process_queue()
            except Exception, err:
                if self._should_log:
                    logging.exception('Failed to process alert queue: %s', err)

    def _process_queue(self):
        # Block for one alert, but not forever. Queues can be remade, so
        # this one might never get something.
        try:
            project_id, name, msg = self._alert_queue.get(timeout=3)
        except Queue.Empty:
            # Try again later
            return

        total = 1
        alerts_by_id = {
            (project_id, name): [1, msg],
        }

        # Grab all queued alerts and condense.
        try:
            while total < 1024 and len(alerts_by_id) < 10:
                project_id, name, msg = self._alert_queue.get(block=False)

                if (project_id, name) not in alerts_by_id:
                    alerts_by_id[(project_id, name)] = [1, msg]
                else:
                    alerts_by_id[(project_id, name)][0] += 1

                total += 1
        except Queue.Empty:
            pass

        for alert_id, alert_data in alerts_by_id.iteritems():
            project_id, name = alert_id
            count, msg = alert_data

            try:
                self._dispatch_alert(project_id, name, msg, count)
            except Exception, err:
                if self._should_log:
                    logging.error(
                        'Failed to deliver alert %s on %s: %s',
                        name, project_id, err)


class Org(object):
    NamePattern = re.compile(r'^[a-zA-Z]([a-zA-Z0-9]|_[a-zA-Z0-9])*$')

    def __init__(self, client, name):
        if not isinstance(client, RoosterClient):
            raise TypeError(
                'Get an Org from a RoosterClient instead of creating one')
        self._client = client

        if not isinstance(name, basestring):
            raise TypeError('Org name must be a %s type' % basestring.__name__)
        elif not self.NamePattern.match(name):
            raise ValueError('Org name must match regex pattern: %s' % (
                self.NamePattern.pattern, ))
        self._name = name

    @property
    def name(self):
        return self._name

    def __getattr__(self, project_name):
        """Get a project from this Org.
        """
        return Project(self._client, self._name, project_name)

    def __getitem__(self, project_name):
        """Get a project from this Org.
        """
        return getattr(self, project_name)


class Project(object):
    """A Rooster Project, where you sent alerts to.
    """

    NamePattern = re.compile(r'^[a-zA-Z]([a-zA-Z0-9]|_[a-zA-Z0-9])*$')

    def __init__(self, client, org_name, name):
        if not isinstance(client, RoosterClient):
            raise TypeError(
                'Get an Org from a RoosterClient instead of creating one')
        self._client = client

        if not isinstance(org_name, basestring):
            raise TypeError('Org name must be a %s type' % basestring.__name__)
        elif not Org.NamePattern.match(org_name):
            raise ValueError('Org name must match regex pattern: %s' % (
                Org.NamePattern.pattern,))
        self._org_name = org_name

        if not isinstance(name, basestring):
            raise TypeError('Project name must be a %s type' % (
                basestring.__name__,))
        elif not self.NamePattern.match(name):
            raise ValueError('Project name must match regex pattern: %s' % (
                self.NamePattern.pattern,))
        self._name = name

    @property
    def org_name(self):
        return self._org_name

    @property
    def name(self):
        return self._name

    def __call__(self, alert_name, msg=None):
        """Trigger an alert, which will send you a notification *if* you
        haven't yet been notified, or if you've cleared it since the laste
        one.

        @param alert_name: A unique identifier for the alert. Only one alert
            sent with this name will actually trigger a notification.
        @param msg: Optional. A description of the alert. This is only stored
            the first time this alert is seen on a stream.
        """
        project_id = '%s/%s' % (self._org_name, self._name)
        self._client.alert(project_id, alert_name, msg=msg)

    def __getattr__(self, alert_name):
        """Get an Alert from this Project.
        """
        return Alert(self._client, self._org_name, self._name, alert_name)

    def __getitem__(self, alert_name):
        """Get an Alert from this Project.
        """
        return getattr(self, alert_name)


class Alert(object):
    """An alert, which can be called to trigger it.
    """

    def __init__(self, client, org_name, project_name, alert_name):
        if not isinstance(client, RoosterClient):
            raise TypeError(
                'Get an Org from a RoosterClient instead of creating one')
        self._client = client

        if not isinstance(org_name, basestring):
            raise TypeError('Org name must be a %s type' % basestring.__name__)
        elif not Org.NamePattern.match(org_name):
            raise ValueError('Org name must match regex pattern: %s' % (
                Org.NamePattern.pattern,))
        self._org_name = org_name

        if not isinstance(project_name, basestring):
            raise TypeError('Project name must be a %s type' % (
                basestring.__name__,))
        elif not Project.NamePattern.match(project_name):
            raise ValueError('Project name must match regex pattern: %s' % (
                Project.NamePattern.pattern,))
        self._project_name = project_name

        self._name = alert_name

    @property
    def org_name(self):
        return self._org_name

    @property
    def project_name(self):
        return self._project_name

    @property
    def name(self):
        return self._name

    def __call__(self, msg=None):
        """Trigger an alert, which will send you a notification *if* you
        haven't yet been notified, or if you've cleared it since the laste
        one.

        @param msg: Optional. A description of the alert. This is only stored
            the first time this alert is seen on a stream.
        """
        project_id = '%s/%s' % (self._org_name, self._project_name)
        self._client.alert(project_id, self._name, msg=msg)


rooster = RoosterClient(
    None,
    async=False,  # Don't start thread unless initialize() does it.
)
